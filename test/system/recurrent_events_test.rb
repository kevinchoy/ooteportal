require "application_system_test_case"

class RecurrentEventsTest < ApplicationSystemTestCase
  setup do
    @recurrent_event = recurrent_events(:one)
  end

  test "visiting the index" do
    visit recurrent_events_url
    assert_selector "h1", text: "Recurrent Events"
  end

  test "creating a Recurrent event" do
    visit recurrent_events_url
    click_on "New Recurrent Event"

    fill_in "Dates", with: @recurrent_event.dates
    fill_in "Description", with: @recurrent_event.description
    fill_in "End time", with: @recurrent_event.end_time
    fill_in "Name", with: @recurrent_event.name
    fill_in "No of duty member", with: @recurrent_event.no_of_duty_member
    fill_in "Start time", with: @recurrent_event.start_time
    fill_in "Tenant", with: @recurrent_event.tenant_id
    fill_in "Venue", with: @recurrent_event.venue_id
    click_on "Create Recurrent event"

    assert_text "Recurrent event was successfully created"
    click_on "Back"
  end

  test "updating a Recurrent event" do
    visit recurrent_events_url
    click_on "Edit", match: :first

    fill_in "Dates", with: @recurrent_event.dates
    fill_in "Description", with: @recurrent_event.description
    fill_in "End time", with: @recurrent_event.end_time
    fill_in "Name", with: @recurrent_event.name
    fill_in "No of duty member", with: @recurrent_event.no_of_duty_member
    fill_in "Start time", with: @recurrent_event.start_time
    fill_in "Tenant", with: @recurrent_event.tenant_id
    fill_in "Venue", with: @recurrent_event.venue_id
    click_on "Update Recurrent event"

    assert_text "Recurrent event was successfully updated"
    click_on "Back"
  end

  test "destroying a Recurrent event" do
    visit recurrent_events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recurrent event was successfully destroyed"
  end
end
