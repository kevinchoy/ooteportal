require "application_system_test_case"

class MemberSchedulesTest < ApplicationSystemTestCase
  setup do
    @member_schedule = member_schedules(:one)
  end

  test "visiting the index" do
    visit member_schedules_url
    assert_selector "h1", text: "Member Schedules"
  end

  test "creating a Member schedule" do
    visit member_schedules_url
    click_on "New Member Schedule"

    fill_in "Member", with: @member_schedule.member_id
    fill_in "Unavailable end date", with: @member_schedule.unavailable_end_date
    fill_in "Unavailable start date", with: @member_schedule.unavailable_start_date
    click_on "Create Member schedule"

    assert_text "Member schedule was successfully created"
    click_on "Back"
  end

  test "updating a Member schedule" do
    visit member_schedules_url
    click_on "Edit", match: :first

    fill_in "Member", with: @member_schedule.member_id
    fill_in "Unavailable end date", with: @member_schedule.unavailable_end_date
    fill_in "Unavailable start date", with: @member_schedule.unavailable_start_date
    click_on "Update Member schedule"

    assert_text "Member schedule was successfully updated"
    click_on "Back"
  end

  test "destroying a Member schedule" do
    visit member_schedules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Member schedule was successfully destroyed"
  end
end
