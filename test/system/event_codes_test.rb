require "application_system_test_case"

class EventCodesTest < ApplicationSystemTestCase
  setup do
    @event_code = event_codes(:one)
  end

  test "visiting the index" do
    visit event_codes_url
    assert_selector "h1", text: "Event Codes"
  end

  test "creating a Event code" do
    visit event_codes_url
    click_on "New Event Code"

    fill_in "Code", with: @event_code.code
    fill_in "Description", with: @event_code.description
    fill_in "Tenant", with: @event_code.tenant_id
    click_on "Create Event code"

    assert_text "Event code was successfully created"
    click_on "Back"
  end

  test "updating a Event code" do
    visit event_codes_url
    click_on "Edit", match: :first

    fill_in "Code", with: @event_code.code
    fill_in "Description", with: @event_code.description
    fill_in "Tenant", with: @event_code.tenant_id
    click_on "Update Event code"

    assert_text "Event code was successfully updated"
    click_on "Back"
  end

  test "destroying a Event code" do
    visit event_codes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Event code was successfully destroyed"
  end
end
