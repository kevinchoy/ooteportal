require 'test_helper'

class RecurrentEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recurrent_event = recurrent_events(:one)
  end

  test "should get index" do
    get recurrent_events_url
    assert_response :success
  end

  test "should get new" do
    get new_recurrent_event_url
    assert_response :success
  end

  test "should create recurrent_event" do
    assert_difference('RecurrentEvent.count') do
      post recurrent_events_url, params: { recurrent_event: { dates: @recurrent_event.dates, description: @recurrent_event.description, end_time: @recurrent_event.end_time, name: @recurrent_event.name, no_of_duty_member: @recurrent_event.no_of_duty_member, start_time: @recurrent_event.start_time, tenant_id: @recurrent_event.tenant_id, venue_id: @recurrent_event.venue_id } }
    end

    assert_redirected_to recurrent_event_url(RecurrentEvent.last)
  end

  test "should show recurrent_event" do
    get recurrent_event_url(@recurrent_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_recurrent_event_url(@recurrent_event)
    assert_response :success
  end

  test "should update recurrent_event" do
    patch recurrent_event_url(@recurrent_event), params: { recurrent_event: { dates: @recurrent_event.dates, description: @recurrent_event.description, end_time: @recurrent_event.end_time, name: @recurrent_event.name, no_of_duty_member: @recurrent_event.no_of_duty_member, start_time: @recurrent_event.start_time, tenant_id: @recurrent_event.tenant_id, venue_id: @recurrent_event.venue_id } }
    assert_redirected_to recurrent_event_url(@recurrent_event)
  end

  test "should destroy recurrent_event" do
    assert_difference('RecurrentEvent.count', -1) do
      delete recurrent_event_url(@recurrent_event)
    end

    assert_redirected_to recurrent_events_url
  end
end
