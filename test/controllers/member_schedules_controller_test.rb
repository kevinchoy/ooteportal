require 'test_helper'

class MemberSchedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @member_schedule = member_schedules(:one)
  end

  test "should get index" do
    get member_schedules_url
    assert_response :success
  end

  test "should get new" do
    get new_member_schedule_url
    assert_response :success
  end

  test "should create member_schedule" do
    assert_difference('MemberSchedule.count') do
      post member_schedules_url, params: { member_schedule: { member_id: @member_schedule.member_id, unavailable_end_date: @member_schedule.unavailable_end_date, unavailable_start_date: @member_schedule.unavailable_start_date } }
    end

    assert_redirected_to member_schedule_url(MemberSchedule.last)
  end

  test "should show member_schedule" do
    get member_schedule_url(@member_schedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_member_schedule_url(@member_schedule)
    assert_response :success
  end

  test "should update member_schedule" do
    patch member_schedule_url(@member_schedule), params: { member_schedule: { member_id: @member_schedule.member_id, unavailable_end_date: @member_schedule.unavailable_end_date, unavailable_start_date: @member_schedule.unavailable_start_date } }
    assert_redirected_to member_schedule_url(@member_schedule)
  end

  test "should destroy member_schedule" do
    assert_difference('MemberSchedule.count', -1) do
      delete member_schedule_url(@member_schedule)
    end

    assert_redirected_to member_schedules_url
  end
end
