require 'test_helper'

class EventCodesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event_code = event_codes(:one)
  end

  test "should get index" do
    get event_codes_url
    assert_response :success
  end

  test "should get new" do
    get new_event_code_url
    assert_response :success
  end

  test "should create event_code" do
    assert_difference('EventCode.count') do
      post event_codes_url, params: { event_code: { code: @event_code.code, description: @event_code.description, tenant_id: @event_code.tenant_id } }
    end

    assert_redirected_to event_code_url(EventCode.last)
  end

  test "should show event_code" do
    get event_code_url(@event_code)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_code_url(@event_code)
    assert_response :success
  end

  test "should update event_code" do
    patch event_code_url(@event_code), params: { event_code: { code: @event_code.code, description: @event_code.description, tenant_id: @event_code.tenant_id } }
    assert_redirected_to event_code_url(@event_code)
  end

  test "should destroy event_code" do
    assert_difference('EventCode.count', -1) do
      delete event_code_url(@event_code)
    end

    assert_redirected_to event_codes_url
  end
end
