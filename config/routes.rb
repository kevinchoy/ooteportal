Rails.application.routes.draw do
  resources :issues
  resources :settings
  resources :teams
  get 'errors/not_found'
  get 'errors/internal_server_error'
  resources :recurrent_events
  resources :events
  resources :event_codes
  resources :event_types
  resources :venues
  resources :member_schedules
  resources :members
  get 'home/index'
  root :to => "home#index"
  
#  namespace :admin do
#      resources :users
#      root to: "users#index"
#    end
#  root to: 'visitors#index'
    
  # *MUST* come *BEFORE* devise's definitions (below)
  as :user do   
    match '/user/confirmation' => 'confirmations#update', :via => :put, :as => :update_user_confirmation
  end

  devise_for :users, :controllers => { 
    :registrations => "registrations",
    :confirmations => "confirmations",
    :sessions => "sessions", 
    :passwords => "milia/passwords", 
  }


  resources :users

  get "personal_data_protection", to: "home#personal_data_protection"
  get "display_all_members", to: "members#display_all_members"
  get "display_upcoming_events", to: "events#display_upcoming_events"
  get "display_past_events", to: "events#display_past_events"
  get "display_events_by_member", to: "members#display_events_by_member"
  get "reactivate_member", to: "members#reactivate_member"
  get "member_assignment/:id", to: "events#member_assignment"
  patch "update_member_assignment/:id", to: "events#update_member_assignment"
  get "search_events", to: "events#search_events"
  get "search_members", to: "members#search_members"
  get "change_tenant/:id", to: "members#change_tenant"
  get "display_team_members", to: "teams#display_team_members"
  get "display_members_report", to: "members#display_members_report"
  get "search_members_report", to: "members#search_members_report"
  get "display_my_events", to: "events#display_my_events"
  get "display_all_events", to: "events#display_all_events"
  get "display_member_unavailable_schedule", to: "member_schedules#display_member_unavailable_schedule"
  get "display_users_by_portal", to: "users#display_users_by_portal"
  get "edit_email", to: "users#edit_email"
  patch "update_email", to:"users#update_email"
  
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  
  patch "update_report", to: "events#update_report"
  patch "update_attendance", to: "events#update_attendance"
  
  
end
