# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_22_153451) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "event_codes", force: :cascade do |t|
    t.string "code"
    t.text "description"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_event_codes_on_tenant_id"
  end

  create_table "event_event_types", force: :cascade do |t|
    t.integer "event_id"
    t.integer "event_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_event_types_on_event_id"
    t.index ["event_type_id"], name: "index_event_event_types_on_event_type_id"
  end

  create_table "event_loggers", force: :cascade do |t|
    t.integer "event_id"
    t.string "event_log"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_event_loggers_on_tenant_id"
  end

  create_table "event_members", force: :cascade do |t|
    t.integer "event_id"
    t.integer "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "sign_on"
    t.datetime "sign_off"
    t.index ["event_id"], name: "index_event_members_on_event_id"
    t.index ["member_id"], name: "index_event_members_on_member_id"
  end

  create_table "event_types", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_event_types_on_tenant_id"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "start_date_time"
    t.datetime "end_date_time"
    t.integer "venue_id"
    t.text "description"
    t.integer "no_of_duty_member"
    t.string "name"
    t.string "status"
    t.integer "recurrent_event_id"
    t.integer "event_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tenant_id"
    t.text "report"
    t.index ["tenant_id"], name: "index_events_on_tenant_id"
  end

  create_table "issues", force: :cascade do |t|
    t.integer "user_id"
    t.string "member_name"
    t.text "description"
    t.text "response"
    t.string "status", default: "Raised"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_loggers", force: :cascade do |t|
    t.integer "tenant_id"
    t.integer "member_id"
    t.string "member_log"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_member_loggers_on_member_id"
    t.index ["tenant_id"], name: "index_member_loggers_on_tenant_id"
  end

  create_table "member_schedules", force: :cascade do |t|
    t.integer "member_id"
    t.datetime "unavailable_start_date"
    t.datetime "unavailable_end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tenant_id"
    t.index ["tenant_id"], name: "index_member_schedules_on_tenant_id"
  end

  create_table "members", force: :cascade do |t|
    t.integer "tenant_id"
    t.integer "user_id"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mobile_number"
    t.string "region"
    t.string "description"
    t.string "street_name"
    t.float "latitude"
    t.float "longitude"
    t.boolean "active_status"
    t.integer "role"
    t.boolean "pdpa"
    t.index ["tenant_id"], name: "index_members_on_tenant_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "recurrent_event_event_types", force: :cascade do |t|
    t.integer "event_type_id"
    t.integer "recurrent_event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_type_id"], name: "index_recurrent_event_event_types_on_event_type_id"
    t.index ["recurrent_event_id"], name: "index_recurrent_event_event_types_on_recurrent_event_id"
  end

  create_table "recurrent_events", force: :cascade do |t|
    t.string "name"
    t.string "dates"
    t.time "start_time"
    t.time "end_time"
    t.integer "venue_id"
    t.text "description"
    t.integer "no_of_duty_member"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_code_id"
    t.index ["tenant_id"], name: "index_recurrent_events_on_tenant_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "setting_loggers", force: :cascade do |t|
    t.integer "setting_id"
    t.string "setting_log"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_setting_loggers_on_tenant_id"
  end

  create_table "settings", force: :cascade do |t|
    t.integer "tenant_id"
    t.boolean "send_sms", default: false
    t.boolean "manage_event_report", default: false
    t.boolean "manage_attendance", default: false
    t.boolean "display_phone", default: false
    t.boolean "manage_event_code", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "display_rhq", default: false
    t.index ["tenant_id"], name: "index_settings_on_tenant_id"
  end

  create_table "team_members", force: :cascade do |t|
    t.integer "member_id"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_team_members_on_member_id"
    t.index ["team_id"], name: "index_team_members_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tenant_id"], name: "index_teams_on_tenant_id"
  end

  create_table "tenants", force: :cascade do |t|
    t.integer "tenant_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_tenants_on_name"
    t.index ["tenant_id"], name: "index_tenants_on_tenant_id"
  end

  create_table "tenants_users", id: false, force: :cascade do |t|
    t.integer "tenant_id", null: false
    t.integer "user_id", null: false
    t.index ["tenant_id", "user_id"], name: "index_tenants_users_on_tenant_id_and_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.boolean "skip_confirm_change_password", default: false
    t.integer "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "role"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.integer "invited_by_id"
    t.integer "invitations_count", default: 0
    t.integer "default_tenant_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["tenant_id"], name: "index_users_on_tenant_id"
  end

  create_table "venues", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "url"
    t.string "street_name"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tenant_id"
    t.index ["tenant_id"], name: "index_venues_on_tenant_id"
  end

end
