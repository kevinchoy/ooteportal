class CreateMemberSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :member_schedules do |t|
      t.integer :member_id
      t.datetime :unavailable_start_date
      t.datetime :unavailable_end_date

      t.timestamps
    end
  end
end
