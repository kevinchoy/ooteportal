class AddPdpaToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :pdpa, :boolean
  end
end
