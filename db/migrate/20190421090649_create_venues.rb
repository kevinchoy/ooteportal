class CreateVenues < ActiveRecord::Migration[5.2]
  def change
    create_table :venues do |t|
      t.string :name
      t.string :description
      t.string :url
      t.string :street_name
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
