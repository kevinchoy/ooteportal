class AddDisplayRhqInSetting < ActiveRecord::Migration[5.2]
  def change
    add_column :settings, :display_rhq, :boolean, default: false
  end
end
