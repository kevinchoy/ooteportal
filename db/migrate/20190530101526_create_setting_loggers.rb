class CreateSettingLoggers < ActiveRecord::Migration[5.2]
  def change
    create_table :setting_loggers do |t|
      t.integer :setting_id
      t.string :setting_log
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
