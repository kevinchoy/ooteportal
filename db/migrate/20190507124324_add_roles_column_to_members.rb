class AddRolesColumnToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :role, :integer
  end
end
