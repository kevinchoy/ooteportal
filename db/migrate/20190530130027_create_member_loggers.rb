class CreateMemberLoggers < ActiveRecord::Migration[5.2]
  def change
    create_table :member_loggers do |t|
      t.references :tenant, foreign_key: true
      t.references :member, foreign_key: true
      t.string :member_log

      t.timestamps
    end
  end
end
