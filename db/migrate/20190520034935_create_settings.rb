class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.references :tenant, foreign_key: true
      t.boolean :send_sms, :default => false
      t.boolean :manage_event_report, :default => false
      t.boolean :manage_attendance, :default => false
      t.boolean :display_phone, :default => false
      t.boolean :manage_event_code, :default => false

      t.timestamps
    end
  end
end
