class CreateEventLoggers < ActiveRecord::Migration[5.2]
  def change
    create_table :event_loggers do |t|
      t.integer :event_id
      t.string :event_log
      t.references :tenant
      t.timestamps
    end
  end
end
