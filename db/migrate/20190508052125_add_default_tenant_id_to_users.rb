class AddDefaultTenantIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :default_tenant_id, :integer
  end
end
