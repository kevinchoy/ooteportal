class AddSignOnOffEventMembers < ActiveRecord::Migration[5.2]
  def change
    if !column_exists?(:event_members, :sign_on)
      add_column :event_members, :sign_on, :datetime
      add_column :event_members, :sign_off, :datetime
    end
  end
end
