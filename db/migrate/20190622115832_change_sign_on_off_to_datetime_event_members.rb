class ChangeSignOnOffToDatetimeEventMembers < ActiveRecord::Migration[5.2]
  def change
    remove_column :event_members, :sign_on
    remove_column :event_members, :sign_off
  end
end
