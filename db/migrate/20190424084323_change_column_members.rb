class ChangeColumnMembers < ActiveRecord::Migration[5.2]
  def change
    rename_column :members, :longtitude, :longitude
  end
end
