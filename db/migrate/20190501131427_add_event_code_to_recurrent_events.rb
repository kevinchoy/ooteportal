class AddEventCodeToRecurrentEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :recurrent_events, :event_code_id, :integer
  end
end
