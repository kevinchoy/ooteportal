class AddTentantToMemberSchedule < ActiveRecord::Migration[5.2]
  def change
    add_reference :member_schedules, :tenant, foreign_key: true
  end
end
