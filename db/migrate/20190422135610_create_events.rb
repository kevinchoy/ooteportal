class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.datetime :start_date_time
      t.datetime :end_date_time
      t.integer :venue_id
      t.text :description
      t.integer :no_of_duty_member
      t.string :name
      t.string :status
      t.integer :recurrent_event_id
      t.integer :event_code_id

      t.timestamps
    end
  end
end
