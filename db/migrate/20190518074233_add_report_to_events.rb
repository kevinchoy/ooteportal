class AddReportToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :report, :text
  end
end
