class AddTenantToVenue < ActiveRecord::Migration[5.2]
  def change
    add_reference :venues, :tenant, foreign_key: true
  end
end
