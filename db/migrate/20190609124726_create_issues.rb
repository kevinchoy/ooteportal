class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.integer :user_id
      t.string :member_name
      t.text :description
      t.text :response
      t.string :status, default: "Raised"

      t.timestamps
    end
  end
end
