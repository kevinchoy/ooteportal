class ChangeActiveStatusToMembers < ActiveRecord::Migration[5.2]
  def change
    change_column :members, :active_status, :boolean
  end
end
