class CreateEventCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :event_codes do |t|
      t.string :code
      t.text :description
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
