class AddAttendanceToEventmembers < ActiveRecord::Migration[5.2]
  def change
    add_column :event_members, :attendance, :boolean, :default => false
  end
end
