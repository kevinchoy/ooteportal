class CreateRecurrentEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :recurrent_events do |t|
      t.string :name
      t.string :dates
      t.time :start_time
      t.time :end_time
      t.integer :venue_id
      t.text :description
      t.integer :no_of_duty_member
      t.references :tenant, foreign_key: true

      t.timestamps
    end
  end
end
