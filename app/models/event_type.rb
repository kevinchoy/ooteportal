class EventType < ApplicationRecord

  acts_as_tenant

  include Hashid::Rails
  
  has_many :event_event_types
  has_many :events, through: :event_event_types
  
  validates_presence_of :name
  validates_uniqueness_of :name, :scope => :tenant_id
  
  has_many :recurrent_event_event_types
  has_many :recurrent_events, through: :recurrent_event_event_types
  
end
