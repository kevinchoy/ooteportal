class RecurrentEventEventType < ApplicationRecord
  belongs_to :event_type
  belongs_to :recurrent_event
end
