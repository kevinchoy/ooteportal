class EventLogger < ApplicationRecord

  acts_as_tenant
  
  has_one :event
  
  include Hashid::Rails
  
  def self.add_log(event_id, log_text)
    EventLogger.create(event_id: event_id, event_log: "[ #{Time.current.strftime("%d-%b-%Y - %l:%M %p")} ] : #{log_text}.")
  end  
  
end
