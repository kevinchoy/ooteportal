class Setting < ApplicationRecord
  acts_as_tenant
  
  def to_setting_string
    "Send SMS: #{send_sms} - 
    Manage Event Report: #{manage_event_report} - 
    Manage Attendance: #{manage_attendance} - 
    Display Phone: #{display_phone} - 
    Manage Event Code: #{manage_event_code}"
  end
end
