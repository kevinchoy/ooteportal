class User < ApplicationRecord

  acts_as_universal_and_determines_account
  has_many :member, :dependent => :destroy
  
  #has_many :tenanted_members, :dependent => :destroy,
  #         :class_name => "Member", :foreign_key => 'user_id'

  enum role: [:user, :admin]
  after_initialize :set_default_role, :if => :new_record?

  include Hashid::Rails
  
  has_many :issues

  def set_default_role
    self.role ||= :user
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :validatable
end
