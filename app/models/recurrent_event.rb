class RecurrentEvent < ApplicationRecord
  acts_as_tenant
  
  belongs_to :venue
  has_many :events
  
  has_many :recurrent_event_event_types, dependent: :destroy
  has_many :event_types, through: :recurrent_event_event_types
  
  validates :name, presence: true
  validates :dates, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
  validates :venue_id, presence: true
  validates :description, presence: true
  validates :no_of_duty_member, presence: true
  #validates :event_code_id, presence: true
  validate :validate_event_code

  
  include Hashid::Rails
  
  def validate_event_code
    if Setting.first.manage_event_code && event_code_id.blank?
      errors.add(:event_code, "can't be blank")
    end
  end
  
end
