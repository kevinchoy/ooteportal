class MemberLogger < ApplicationRecord
  acts_as_tenant
  
  has_one :member
  
  include Hashid::Rails
  
  def self.add_log(member_id, log_text)
    MemberLogger.create(member_id: member_id, member_log: "[ #{Time.current.strftime("%d-%b-%Y - %l:%M %p")} ] : #{log_text}.")
  end  
end
