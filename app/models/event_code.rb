class EventCode < ApplicationRecord
  belongs_to :tenant
  
  acts_as_tenant

  include Hashid::Rails
  
  validates_presence_of :code
  validates_uniqueness_of :code, :scope => :tenant_id

  
    def event_code_descriptor
      "#{code} - #{description}"
  end
  
end
