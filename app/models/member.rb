class Member < ApplicationRecord
   
  belongs_to :user
  has_many :member_schedules
  
  has_many :event_members
  has_many :events, through: :event_members
  
  has_many :team_members
  has_many :teams, through: :team_members

  geocoded_by :street_name, latitude: :latitude, longitude: :longitude
  after_validation :geocode

  include Hashid::Rails
  include ApplicationHelper
  
  
  validates :mobile_number, presence: true, length: {minimum: 8, maximum: 8}
  validates :street_name, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :region, presence: true
  validates :role, presence: true
  validates :team_ids, presence: true, unless: :skip_team_validation
  validates_acceptance_of :pdpa, accept: true
  
  attribute :active_status, default: true
  
  attr_accessor :skip_team_validation

  acts_as_tenant
  #acts_as_universal 
  
  DEFAULT_ADMIN = {
    first_name: "Admin",
    last_name:  "Please edit me",
    role: :admin,
    active_status: true
  }
  
  enum role: [:user, :team_leader, :admin]

  def self.create_new_member(user, params)
    # add any other initialization for a new member
    # return user.create_member( params )
    return Member.create_member(user, params)
  end

  def self.create_org_admin(user)
    new_member = create_new_member(user, DEFAULT_ADMIN)
    unless new_member.errors.empty?
      raise ArgumentError, new_member.errors.full_messages.uniq.join(", ")
    end

    Team.create(name: "General", description: "General")
    Setting.create(send_sms: false) if Setting.first.blank?

    return new_member
      
  end
  
  def full_name
    first_name + " " + last_name
  end


  def self.available_members(start_date, end_date, event_id, current_user)
    @members = Array.new
    current_date_range = start_date..end_date
    
    venue_coordinates = Event.find(event_id).convert_to_coordinates
    members_by_distance = Member.near(venue_coordinates, 100, unit: :km, order: 'distance')
    members_by_distance.each do |member|
        @members << member if ((!member.events.any? { |event| event.date_range.overlaps?(current_date_range) && 
        event.status != "Cancelled" } && 
        !member.member_schedules.any? { |unavailable| unavailable.date_range.overlaps?(current_date_range) } &&
        member.active_status? &&
        (member.teams.any? { |team| current_user.member.first.teams.exists?(team.id) } || current_user.member.first.try(:admin?))) || 
        member.events.any? { |event| event.id == event_id })
    end
    
    
    return @members

  end

  def convert_to_coordinates
    
    coordinates << latitude
    coordinates << longtitude
    
  end
  
  def member_descriptor
    "#{full_name} ( #{ Setting.first.display_rhq ? 'RHQ ' + region + " ," : ""} #{(teams.map { |t| t.name }.join' , ')} )  "
  end
  
  def send_sms(event, tenant)
    require 'messagebird'
    client = MessageBird::Client.new(ENV["MESSAGEBIRD_API"])
    response = client.message_create(tenant.truncate_words(1, omission: "")[0..10], "+65#{mobile_number}",
      "Hello #{first_name}, gentle reminder upcoming duty #{event.name} on #{event.start_date_time.strftime(Event.date_format)}. Please log-on to #{tenant} Portal to find out more."
    )
  end
  
  def self.create_member (user, params)
    @member = Member.new(params)
    @member.user = user
    @member.save(validate: false)
    @member
  end
  
  def self.search_members(search_params)
    
    name = search_params[:name].present? ? search_params[:name].downcase : nil
    team = search_params[:team].present? ? search_params[:team] : nil

    if !name.present? && !team.present?
      Member.all.paginate(page: search_params[:page], per_page: 10).order(:first_name)
    elsif name.present? && !team.present?
      Member.includes(:teams).paginate(page: search_params[:page], per_page: 10).where("(lower(first_name) LIKE ? 
        OR lower(last_name) LIKE ?
        OR (lower(first_name) || ' ' || lower(last_name)) LIKE ?)",
        "%#{name}%", "%#{name}%", "%#{name}%").references(:teams).order(:first_name) 
    else
      Member.includes(:teams).paginate(page: search_params[:page], per_page: 10).where("(lower(first_name) LIKE ? 
        OR lower(last_name) LIKE ?
        OR (lower(first_name) || ' ' || lower(last_name)) LIKE ?) 
        AND teams.id #{ !team.present? ? 'IS ' : '=' } ?",
        "%#{name}%", "%#{name}%", "%#{name}%", team).references(:teams).order(:first_name) 
    end
  
  end
  
  def self.search_members_report(search_params)
    from_start_date = search_params[:from_start_date].present? ? search_params[:from_start_date].to_time : 6.months.ago
    to_start_date =  search_params[:to_start_date].present? ? search_params[:to_start_date].to_time : 1.days.from_now
    team = search_params[:team].present? ? search_params[:team] : nil
    
    
    if team.present? 
      Member.includes(:teams, :events).where("
        (events.start_date_time BETWEEN ? AND ?) 
        AND teams.id #{ !team.present? ? 'IS NOT' : '=' } ?",
        "#{from_start_date}", "#{to_start_date}", team).references(:teams, :events).order("first_name")
    else
      Member.includes(:teams, :events).where("
        (events.start_date_time BETWEEN ? AND ?)",
        "#{from_start_date}", "#{to_start_date}",).references(:teams, :events).order("first_name") 
    end
  end
  
  def to_member_string
    "First Name: #{first_name} - 
    Last Name: #{last_name} - 
    mobile_number: #{mobile_number} - 
    Region: #{region} - 
    Street Name: #{street_name} - 
    Active Status: #{active_status} - 
    Role: #{role} - 
    PDPA: #{pdpa}"
  end

end
