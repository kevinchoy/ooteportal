class MemberSchedule < ApplicationRecord
  acts_as_tenant
  belongs_to :member

  include Hashid::Rails
  
  validates_presence_of :unavailable_start_date, :unavailable_end_date, :member_id
  
  def date_range
    unavailable_start_date..unavailable_end_date
  end
  
end
