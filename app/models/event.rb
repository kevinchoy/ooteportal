class Event < ApplicationRecord
  include ApplicationHelper
  acts_as_tenant
  
  has_one_attached :attachment
  
  has_many :event_event_types, dependent: :destroy
  has_many :event_types, through: :event_event_types
  
  belongs_to :venue
  
  has_many :event_members, dependent: :destroy
  has_many :members, through: :event_members
  
  has_one :recurrent_event
  
  belongs_to :event_code
  
  has_many :event_loggers, dependent: :destroy

  validates :name, presence: true, length: {minimum: 3}
  validates :start_date_time, presence: true
  validates :end_date_time, presence: true
  validates :no_of_duty_member, presence: true, numericality: { only_integer: true }
  validates :event_types, presence: true
  validate :validate_event_code
  validates :venue_id, presence: true
  validate :validate_schedule_for_conflict
  
  include Hashid::Rails
  
  def validate_event_code
    if Setting.first.manage_event_code && event_code.blank?
      errors.add(:event_code, "can't be blank")
    end
  end
  
  
  def self.create_event(recurrent_event, event_date, full_name)

    event_start_date = (event_date.to_s + " " +  (recurrent_event.start_time).strftime(Event.time_format)).to_datetime.strftime(Event.date_format)
    event_end_date = (event_date.to_s + " " +  (recurrent_event.end_time).strftime(Event.time_format)).to_datetime.strftime(Event.date_format)

    @event = Event.new(name: recurrent_event.name, 
                  description: recurrent_event.description, 
                  start_date_time: event_start_date, 
                  end_date_time: event_end_date, 
                  venue_id: recurrent_event.venue_id,
                  no_of_duty_member: recurrent_event.no_of_duty_member,
                  status: "Not assigned",
                  event_code_id: recurrent_event.event_code_id,
                  recurrent_event_id: recurrent_event.id,
                  event_type_ids: recurrent_event.event_type_ids)
   @event.save!
   EventLogger.add_log(@event.id, "Event created by #{full_name}")
                  
  end

  def check_status(member_list, no_of_duty_member)
    if member_list.present?
      member_list = member_list.reject { |m| m.empty? }
      if member_list.count == 0
        "Not assigned"
      elsif member_list.size < no_of_duty_member.to_i
        "Partially assigned"
      else
        "Assigned"
      end
    else
      "Not assigned"
    end
  end
  
  def date_range
    start_date_time..end_date_time
  end
  
  def self.send_reminder
    @tenants = Tenant.all
    @tenants.each do |tenant|
      Tenant.set_current_tenant(tenant.id)
      
      @due_events = Event.where(start_date_time: 2.days.from_now..3.days.from_now)
        @due_events.each do |event|
          event.members.each do |member|
            EventMailer.with(event: event, member: member, tenant: Tenant.current_tenant.id).event_reminder_email.deliver_now
            tenant = Tenant.current_tenant.name
            member.send_sms(event, tenant) if Setting.first.send_sms
          end
        end
    end
  end
  
  def self.send_unassigned_event_reminder
    @tenants = Tenant.all
    
    @tenants.each do |tenant|
      
      Tenant.set_current_tenant(tenant.id)
      @unassigned_events = Event.where("(status = 'Not assigned' OR status = 'Partially assigned') AND (start_date_time BETWEEN ? AND ?)", 2.days.from_now, 7.days.from_now)
      @admin_members = Member.where(role: :admin)
      
      @unassigned_events.each do |event|
        @admin_members.each do |member|
            EventMailer.with(event: event, member: member,tenant: Tenant.current_tenant.id).event_unassigned_reminder_email.deliver_now
        end
      end
    end
  end
  
  def self.send_new_event_email(event, tenant)
    
    event.members.each do |member|
      EventMailer.with(event: event, member: member, tenant: tenant).new_event_email.deliver_later  
    end
    
  end
  
  def self.send_update_event_email(event, tenant)
    
    event.members.each do |member|
      EventMailer.with(event: event, member: member, tenant: tenant).update_event_email.deliver_now
    end
    
  end
  
  def self.send_cancel_event_email(event, tenant)
    
    event.members.each do |member|
      EventMailer.with(event: event, member: member, tenant: tenant).cancel_event_email.deliver_now
    end
    
  end  
  
  def self.send_new_event_creation_email(event, tenant)
    
    @admin_members = Member.where(role: :admin)
    
    @admin_members.each do |member|
      EventMailer.with(event: event, member: member, tenant: tenant).new_event_creation_email.deliver_now
    end
    
  end
  
  def self.send_unassigned_member_email(event, existing_members, tenant)
    
    unassigned_members = existing_members - event.members.as_json
    unassigned_members.each do |member|
      EventMailer.with(event: event, member: member, tenant: tenant).unassigned_members_from_event_email.deliver_now
    end
  end
  
  def self.search_events(search_params)
    
    from_start_date = search_params[:from_start_date].present? ? search_params[:from_start_date].to_time : 6.months.ago
    to_start_date =  search_params[:to_start_date].present? ? search_params[:to_start_date].to_time : 6.months.from_now
    venue = search_params[:venue].present? ? search_params[:venue] : nil
    status = search_params[:status].present? ? search_params[:status] : nil

    
    if Setting.first.manage_event_code
      event_code = search_params[:event_code].present? ? search_params[:event_code] : nil
      Event.paginate(page: search_params[:page], per_page: 10).where("lower(name) LIKE ? 
            AND (start_date_time BETWEEN ? AND ?) 
            AND venue_id #{ !venue.present? ? 'IS NOT' : '=' } ? 
            AND (event_code_id #{ !event_code.present? ? 'IS NULL OR event_code_id IS NOT' : '=' } ?) 
            AND status #{!status.present? ? 'IS NOT' : '=' } ? ", 
            "%#{search_params[:event].downcase}%", "#{from_start_date}", "#{to_start_date}", venue, event_code, status).order('start_date_time') 
    else
      Event.paginate(page: search_params[:page], per_page: 10).where("lower(name) LIKE ? 
            AND (start_date_time BETWEEN ? AND ?) 
            AND venue_id #{ !venue.present? ? 'IS NOT' : '=' } ? 
            AND status #{!status.present? ? 'IS NOT' : '=' } ? ", 
            "%#{search_params[:event].downcase}%", "#{from_start_date}", "#{to_start_date}", venue, status).order('start_date_time')       
    end
         
  
  end


  def start_time
    start_date_time  
  end
  
  def end_time
    end_date_time
  end
  
  def status_color
    case status
    when "Not assigned"
      "danger"
    when "Partially assigned"
      "warning"
    when "Assigned"
      "success"
    when "Cancelled"
      "secondary"
    end  
  end
  
  def convert_to_coordinates
    
    coordinates = Array.new
    coordinates << venue.latitude
    coordinates << venue.longitude
    
  end
  
  def to_eventstring
    "Event Name: #{name} - 
    Description: #{description} - 
    Start Date and Time:  #{start_date_time.strftime(date_format)} - 
    End Date and Time: #{end_date_time.strftime(date_format)} - 
    Venue: #{venue.name} - 
    Status: #{status} - 
    No of Duty Members: #{no_of_duty_member} - 
    Event Code: #{!event_code.blank? ? event_code.code : 'N/A'}"
    #Event Type: #{to_eventtypes_string}"
  end
  
  def to_eventtypes_string
    event_type_string = ""
    event_types.each do |event_type|
      event_type_string = event_type_string + event_type.name + ", "
    end
    return event_type_string
  end
  
  def to_members_string
    members_string = ""
    members.each do |member|
      members_string = members_string + member.full_name + ", "
    end
    return members_string
  end
  
  def self.date_format
    "%d-%b-%Y - %l:%M %p"
  end
  
  def self.date_with_day_format
    "%d-%b-%Y - %l:%M %p ( %a )"
  end
  
  def self.date_only_format
    "%d-%b-%Y"
  end
  
  def self.time_format
    "%l:%M %p"
  end
  
  def live_event_date_range
    (start_date_time - 1.hours)..(end_date_time + 2.days)
  end
  
  protected
  

    def validate_schedule_for_conflict
      
      members = Member.find(member_ids)
      members.each do |member|
        errors.add(:start_date_time, "for #{member.full_name} is conflicting with another event.") if member.events.any? { |event| event.date_range.overlaps?(date_range) && event.date_range != date_range && event.status != "Cancelled"}
        errors.add(:start_date_time, "for #{member.full_name} is not available for member.") if member.member_schedules.any? { |unavailable| unavailable.date_range.overlaps?(date_range) }
    end

    
  end
  
end
