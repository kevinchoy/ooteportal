module ApplicationHelper
  
  ALERT_TYPES = [:success, :info, :warning, :danger] unless const_defined?(:ALERT_TYPES)

  def bootstrap_flash(options = {})
    flash_messages = []
    flash.each do |type, message|
      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
      next if message.blank?

      type = type.to_sym
      type = :success if type == :notice
      type = :danger  if type == :alert
      type = :danger  if type == :error
      next unless ALERT_TYPES.include?(type)

      tag_class = options.extract!(:class)[:class]
      tag_options = {
        class: "alert fade in alert-#{type} #{tag_class}"
      }.merge(options)

      close_button = content_tag(:button, raw("&times;"), type: "button", class: "close", "data-dismiss" => "alert")

      Array(message).each do |msg|
        text = content_tag(:div, close_button + msg, tag_options)
        flash_messages << text if msg
      end
    end
    flash_messages.join("\n").html_safe
  end
  
  def resource_name
  
    :user
  
  end
  
  def resource
  
    @resource ||= User.new
  
  end
  
  def devise_mapping
  
    @devise_mapping ||= Devise.mappings[:user]
  
  end
  
  def date_format
    "%d-%b-%Y - %l:%M %p"
  end
  
  def date_only_format
    "%d-%b-%Y"
  end
  
  def time_format
    "%l:%M %p"
  end
  
  def event_code_format
    "%Y%m%d"
  end
  
  def confirm_delete_message
    "Are you sure? This action is not reversible!"
  end
  
  def back_button
    link_to "<i class='fa fa-arrow-left'></i>".html_safe, :back, class: "btn btn-secondary btn-sm rounded-0"
  end
  
  def submit_button
    "btn btn-primary btn-sm rounded-0"
  end
  
  def cancel_button
    "btn btn-danger btn-sm rounded-0"
  end
  
  def transparent_button
    "btn bg-white text-primary border-primary rounded-0"
  end

  
end
