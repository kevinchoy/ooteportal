module VenuesHelper
  def get_coordinates(street_name)
    street_name = "Singapore " + street_name
    begin
      Geocoder.search(street_name).first.coordinates
    rescue StandardError => e
      "is not valid. Check that you enter street name only."
    end
  end
end
