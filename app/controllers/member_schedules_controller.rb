class MemberSchedulesController < ApplicationController
  before_action :set_member_schedule, only: [:show, :edit, :update, :destroy]
  before_action :validates_active_status
  before_action :authorized_admin_team_leader, only: [:display_member_unavailable_schedule]


  # GET /member_schedules
  # GET /member_schedules.json
  def index
    @member_schedules = current_user.member.first.member_schedules
  end

  # GET /member_schedules/1
  # GET /member_schedules/1.json
  def show
  end

  # GET /member_schedules/new
  def new
    @member_schedule = MemberSchedule.new
    if params[:mode] == "other"
      authorized_admin_team_leader
    end
  end

  # GET /member_schedules/1/edit
  def edit
  end

  # POST /member_schedules
  # POST /member_schedules.json
  def create
    @member_schedule = MemberSchedule.new(member_schedule_params)
    @member_schedule.member_id = params[:mode] == "other" ? params[:member_schedule][:member_id] : current_user.member.first.id
    
    respond_to do |format|
      if @member_schedule.save
        format.html { redirect_to params[:mode] == "other" ? display_member_unavailable_schedule_path : member_schedules_path, notice: 'Unavailable schedule was successfully added.' }
        format.json { render :show, status: :created, location: @member_schedule }
      else
        format.html { render :new }
        format.json { render json: @member_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /member_schedules/1
  # PATCH/PUT /member_schedules/1.json
  def update
    respond_to do |format|
      if @member_schedule.update(member_schedule_params)
        format.html { redirect_to params[:mode] == "other-edit" ? display_member_unavailable_schedule_path : member_schedules_path, notice: 'Unavailable schedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @member_schedule }
      else
        format.html { render :edit }
        format.json { render json: @member_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /member_schedules/1
  # DELETE /member_schedules/1.json
  def destroy
    @member_schedule.destroy
    respond_to do |format|
      format.html { redirect_to member_schedules_url, notice: 'The selected unavailable schedule was successfully deleted.' }
      format.json { head :no_content }
    end
  end
  
  def display_member_unavailable_schedule
    @member_unavailable_schedule = MemberSchedule.all.order(:unavailable_start_date).paginate(page: params[:page], per_page: 30)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member_schedule
      @member_schedule = MemberSchedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_schedule_params
      params.require(:member_schedule).permit(:member_id, :unavailable_start_date, :unavailable_end_date)
    end
end
