class RecurrentEventsController < ApplicationController
  before_action :set_recurrent_event, only: [:show, :edit, :update, :destroy]
  before_action :authorized_admin_team_leader
  before_action :validates_active_status

  # GET /recurrent_events
  # GET /recurrent_events.json
  def index
    @recurrent_events = RecurrentEvent.all.paginate(page: params[:page], per_page: 10)
  end

  # GET /recurrent_events/1
  # GET /recurrent_events/1.json
  def show
  end

  # GET /recurrent_events/new
  def new
    @recurrent_event = RecurrentEvent.new
  end

  # GET /recurrent_events/1/edit
  def edit
  end

  # POST /recurrent_events
  # POST /recurrent_events.json
  def create
    @recurrent_event = RecurrentEvent.new(recurrent_event_params)

    respond_to do |format|
      if @recurrent_event.save
        event_dates = @recurrent_event.dates.split(",")
        event_dates.each do |event_date|
          Event.create_event(@recurrent_event, event_date, current_user.member.first.full_name)
        end
        format.html { redirect_to @recurrent_event, notice: 'Recurrent event was successfully created.' }
        format.json { render :show, status: :created, location: @recurrent_event }
      else
        format.html { render :new }
        format.json { render json: @recurrent_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recurrent_events/1
  # PATCH/PUT /recurrent_events/1.json
  def update
    respond_to do |format|
      if @recurrent_event.update(recurrent_event_params)
        format.html { redirect_to @recurrent_event, notice: 'Recurrent event was successfully updated.' }
        format.json { render :show, status: :ok, location: @recurrent_event }
      else
        format.html { render :edit }
        format.json { render json: @recurrent_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recurrent_events/1
  # DELETE /recurrent_events/1.json
  def destroy
    @recurrent_event.destroy
    respond_to do |format|
      format.html { redirect_to recurrent_events_url, notice: 'Recurrent event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recurrent_event
      @recurrent_event = RecurrentEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recurrent_event_params
      params.require(:recurrent_event).permit(:name, :dates, :start_time, :end_time, :venue_id, :description, :no_of_duty_member, :event_code_id, :tenant_id,  event_type_ids:[])
    end
end
