class EventsController < ApplicationController
  before_action :authorized_admin_team_leader, only: [:edit, :update, :destroy, :new]
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :validates_active_status

  # GET /events
  # GET /events.json
  def index
    #@events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event_logs = EventLogger.where(event_id: @event.id).order(created_at: :desc)
    @event_member = @event.event_members.where(member: current_user.member.first.id)
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
    if @event.status == "Cancelled" || @event.start_date_time < Time.current
      respond_to do |format|
        format.html { redirect_to @event, notice: "#{@event.name} is either cancelled or over." }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.status = @event.check_status(event_params[:member_ids], event_params[:no_of_duty_member])
    @event.recurrent_event_id = 0

    respond_to do |format|
      if @event.save
        Event.send_new_event_creation_email(@event, Tenant.current_tenant.id)
        EventLogger.add_log(@event.id, "Event created by #{current_user.member.first.full_name}")
        format.html { redirect_to action: "member_assignment", id: @event.hashid, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    
    old_event = @event.to_eventstring
    @event.status = @event.check_status(@event.members.as_json, params[:event][:no_of_duty_member])
    
    respond_to do |format|
      if @event.update(event_params)

        #@event.attachment.attach(params[:attachment]) unless params[:attachment].blank?
        
        Event.send_update_event_email(@event, Tenant.current_tenant.id)
        EventLogger.add_log(@event.id, "Event details updated by #{current_user.member.first.full_name} - [ Previous Details -> #{old_event} ] - [ New Details -> #{@event.to_eventstring} ]")
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    #@event.destroy
    respond_to do |format|
      if(@event.update(status: "Cancelled"))
        Event.send_cancel_event_email(@event, Tenant.current_tenant.id)
        EventLogger.add_log(@event.id, "Event cancelled by #{current_user.member.first.full_name}")
        format.html { redirect_to @event, notice: 'Event was successfully cancelled.' }
        format.json { head :no_content }
      else
        format.html { render :show }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def display_upcoming_events
    @upcoming_events = current_user.member.first.events.paginate(page: params[:page], per_page: 10).where("start_date_time >= ? AND status != ?", Time.current, "Cancelled").order(start_date_time: :asc).order('start_date_time')
  end
  
  def display_past_events
    @past_events = current_user.member.first.events.paginate(page: params[:page], per_page: 10).where("start_date_time < ? AND status != ?", Time.current, "Cancelled").order(start_date_time: :desc).order('start_date_time')
  end
  
  def member_assignment
    
  end

  def update_member_assignment
    @event = Event.find(params[:id])
    existing_members = @event.members.as_json
    old_members = @event.to_members_string
    status = @event.status
  
    respond_to do |format|
      @event.status = @event.check_status(event_params[:member_ids], @event.no_of_duty_member)
      if @event.update(event_params)
        if status == "Not assigned"
          Event.send_new_event_email(@event, Tenant.current_tenant.id)
        else
          Event.send_update_event_email(@event, Tenant.current_tenant.id)
          
        end
        
        Event.send_unassigned_member_email(@event, existing_members, Tenant.current_tenant.id)
        EventLogger.add_log(@event.id, "Member assignment updated by #{current_user.member.first.full_name} - [ Previous Members -> #{old_members} ] - [ New Members -> #{@event.to_members_string} ]")
        format.html { redirect_to @event, notice: 'Members were successfully assigned. Emails will be sent to members.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :member_assignment }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end  
  end
  
  def search_events
    
    @events = Event.search_events(params)
    if @events
      respond_to do |format|
        format.js {render partial: 'events/display_events', obj: @events }
      end
    end

  end
  
  def update_report
    @event = Event.find(params[:event][:id])
    old_report = Sanitize.clean(@event.report)
    respond_to do |format|
      if @event.update(event_params)
        EventLogger.add_log(@event.id, "Event report updated by #{current_user.member.first.full_name}  - [ Previous Report -> #{old_report} ] - [ New Report -> #{Sanitize.clean(@event.report)} ]")
        format.html { redirect_to @event, notice: 'Event report was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :show }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_attendance
    @event = Event.find(params[:event][:id])
    @event_member = @event.event_members.where(member: current_user.member.first.id)
    respond_to do |format|
      if @event_member.first.sign_on.blank? ? @event_member.first.update(sign_on: Time.current) : @event_member.first.update(sign_off: Time.current)
        
        EventLogger.add_log(@event.id, "#{current_user.member.first.full_name} signed #{@event_member.first.sign_off.blank? ? 'on' : 'off'} for this event.")
        format.html { redirect_to @event, notice: "Your sign-#{@event_member.first.sign_off.blank? ? 'on' : 'off'} has been registered." }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :show }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def display_my_events
    @label = "My".upcase
    @events = current_user.member.first.events
    if @events
      respond_to do |format|
        format.js {render partial: 'events/display_home_events', obj: @events }
      end
    end
  end
  
  def display_all_events
    @label = "All".upcase
    @events = Event.all
    if @events
      respond_to do |format|
        format.js {render partial: 'events/display_home_events', obj: @events }
      end
    end
  end
  
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:start_date_time, :end_date_time, :venue_id, :description, :no_of_duty_member, :name, :status, :recurrent_event_id, :tenant_id, :event_code_id, :report, :attachment, event_type_ids:[], member_ids:[])
    end
end
