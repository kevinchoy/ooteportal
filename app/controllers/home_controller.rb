class HomeController < ApplicationController
  skip_before_action :authenticate_tenant!, :only => [ :index ]
  
  

  def index
    #sign_out current_user
    if current_user

      if session[:tenant_id]
        Tenant.set_current_tenant session[:tenant_id]
      else
        Tenant.set_current_tenant current_user.tenants.first
      end
      
      if current_user.member.first.mobile_number.blank? || current_user.member.first.street_name.blank? || current_user.member.first.pdpa.blank?
        flash[:warning] = "Please complete your profile."
        redirect_to edit_user_registration_path(current_user)
      else
        @tenant = Tenant.current_tenant
        params[:tenant_id] = @tenant.id
        
        #Upcoming Event
        @upcoming_events = current_user.member.first.events.where("end_date_time >= ?", Time.current).order(start_date_time: :asc)
        #Check if can take attendance for upcoming
        @event_member = @upcoming_events.empty? ? nil : @upcoming_events.first.event_members.where(member: current_user.member.first.id)
        @upcoming_event = @event_member.blank? ? nil : @upcoming_events.first
        
      end
      
      @issues = Issue.where(status: "Raised") if Tenant.current_tenant.name == "Landlord"
      @tenants = Tenant.where.not(name: "Landlord").order(:name)
    end
  end
  
  def personal_data_protection
    
  end
  
end