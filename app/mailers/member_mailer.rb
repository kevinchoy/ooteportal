class MemberMailer < ApplicationMailer
  
  def new_invite_memmber_email
    @m = params[:member]
    make_bootstrap_mail(to: @m.first.user.email, subject: "[ #{Tenant.current_tenant.name} ] Welcome Onboard!")
  end
  
  def new_invite_tenant_admin_email
    @m = params[:member]
    make_bootstrap_mail(to: @m.user.email, subject: "[ #{Tenant.current_tenant.name} ] Welcome Onboard!")
  end
  
end
