class ApplicationMailer < ActionMailer::Base
  default from: 'do-not-reply@ooteportal.com'
  layout 'mailer'
end
