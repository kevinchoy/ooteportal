json.extract! recurrent_event, :id, :name, :dates, :start_time, :end_time, :venue_id, :description, :no_of_duty_member, :tenant_id, :created_at, :updated_at
json.url recurrent_event_url(recurrent_event, format: :json)
