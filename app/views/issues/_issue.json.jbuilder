json.extract! issue, :id, :user_id, :member_name, :description, :response, :status, :created_at, :updated_at
json.url issue_url(issue, format: :json)
