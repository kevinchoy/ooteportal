json.extract! event, :id, :start_date_time, :end_date_time, :venue_id, :description, :no_of_duty_member, :name, :status, :recurrent_event_id, :event_code_id, :created_at, :updated_at
json.url event_url(event, format: :json)
