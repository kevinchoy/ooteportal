json.extract! team, :id, :name, :description, :tenant_id, :created_at, :updated_at
json.url team_url(team, format: :json)
