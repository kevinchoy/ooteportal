json.extract! setting, :id, :tenant_id, :send_sms, :manage_event_report, :manage_attendance, :display_phone, :manage_event_code, :created_at, :updated_at
json.url setting_url(setting, format: :json)
