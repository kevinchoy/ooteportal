json.extract! member_schedule, :id, :member_id, :unavailable_start_date, :unavailable_end_date, :created_at, :updated_at
json.url member_schedule_url(member_schedule, format: :json)
