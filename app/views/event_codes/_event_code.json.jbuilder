json.extract! event_code, :id, :code, :description, :tenant_id, :created_at, :updated_at
json.url event_code_url(event_code, format: :json)
